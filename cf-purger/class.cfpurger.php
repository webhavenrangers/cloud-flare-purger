<?php

/**
 * Created by PhpStorm.
 * User: kdv
 * Date: 14.03.2016
 * Time: 17:11
 */
class Cfpurger {
	const PAGE_TITILE = 'Cloud Flare Purger Settings';
	const PLUGIN_TITILE = 'Cloud Flare Purger';
	const MENU_TITILE = 'CF Purger';
	const MENU_SLUG = 'cf-purger';
	static $options = array();
	static $options_name = "cf-purger";
	static $defaults = array(
		"enabled" => 1,
		"API_key" => false,
		"email"   => false,
		"domain"  => false,
	);

	public static function init() {
		new Cfpurger();
	}

	function __construct() {
		$this->get_options();
		add_action( 'admin_menu', array( $this, 'load_menu' ) );
		add_action( 'admin_init', array( $this, 'page_init' ) );
		if ( isset( self::$options['API_key'] ) && self::$options['API_key'] ) {
			add_action( 'admin_bar_menu', array( $this, 'custom_adminbar_menu' ), 15 );
		}
		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'register_scripts_styles' ) );
		add_action( 'wp_ajax_purge_cf_cache', array( $this, 'purge_cf_cache' ) );
		add_action( 'wp_ajax_nopriv_purge_cf_cache', array( $this, 'purge_cf_cache' ) );
		add_action( 'admin_notices', array( $this, 'sample_admin_notice_info' ) );
		add_filter( 'plugin_action_links_' . CFPURGER_PLUGIN_BASENAME, array( $this, 'plugin_settings_link' ), 10, 2 );
	}

	function plugin_settings_link($links ) {
		$settings_link = '<a href="admin.php?page='.self::MENU_SLUG.'">Settings</a>';
		array_unshift($links, $settings_link);
		return $links;
	}

	function custom_adminbar_menu( $meta = true ) {
		global $wp_admin_bar;
		if ( ! is_user_logged_in() ) {
			return;
		}
		if ( ! is_super_admin() || ! is_admin_bar_showing() ) {
			return;
		}
		$wp_admin_bar->add_menu( array(
			'parent' => 'top-secondary',
			'id'     => self::MENU_SLUG . '-clear',
			'title'  => __( 'Purge', self::MENU_SLUG ),
			'href'   => '#',
			'meta'   => array(
				'title'    => '',
				'class'    => 'clear',
				'tabindex' => - 1,
			)
		) );
	}

	static function store_options( $name, $options ) {
		add_option( $name, $options ) OR update_option( $name, $options );
	}

	function get_options() {
		self::$options = get_option( self::$options_name );
	}

	function register_scripts_styles() {
		wp_enqueue_style( self::MENU_SLUG . 'style', plugins_url( 'style.css', __FILE__ ) );
		wp_enqueue_script( self::MENU_SLUG . 'script', plugins_url( '/js/script.js', __FILE__ ), array( 'jquery' ),
			true );
	}

	function load_menu() {
		add_menu_page( self::PAGE_TITILE, self::MENU_TITILE, 'edit_others_posts', self::MENU_SLUG,
			array( $this, 'create_admin_page' ), plugins_url( 'images/cf-icon.png', __FILE__ ), 6 );
	}

	public static function plugin_activation() {
		Cfpurger::store_options( self::$options_name, self::$defaults );
	}

	public static function plugin_deactivation() {
		delete_option( Cfpurger::$options_name );
	}

	function sample_admin_notice_info() {
		$this->get_options();
		if ( isset( $_GET['page'] ) && $_GET['page'] == self::MENU_SLUG && (self::$options['API_key'] == '' || self::$options['email'] == '' || self::$options['email'] == false ) ): ?>
			<div class="notice notice-success is-dismissible">
			<p><?php _e( 'You will need to enter your API key and Email Address for your account first. You can
                    locate the API key by going to your <a target="_blank" href="https://www.cloudflare.com/my-account.html">Account</a>',
					'cf-purger' ); ?></p>
			</div><?php
		endif;
	}

	public function create_admin_page() {
		// Set class property
		$this->get_options(); ?>
		<div class="wrap">
			<form method="post" action="options.php"><?php
				// This prints out all hidden setting fields
				settings_fields( self::$options_name . '_group' );
				do_settings_sections( self::MENU_SLUG );
				if( self::$options['API_key'] !== false && !empty(self::$options['API_key']) )
					$this->render_purge_button();
				submit_button(); ?>
			</form>
		</div>
		<?php
	}

	function render_purge_button() {
		echo '<button id="purge_cloud_flare" onclick="jQuery(\'#wp-admin-bar-cf-purger-clear\').click(); return false;" class="ab-item button-primary" tabindex="-1" href="#">Purge  <span class="dashicons dashicons-cloud"></span></span></button>';
	}

	/**
	 * Register and add settings
	 */
	public function page_init() {
		register_setting( self::$options_name . '_group', // Option group
			self::$options_name, // Option name
			array( $this, 'sanitize' ) // Sanitize
		);

		add_settings_section( 'setting_section_id', // ID
			self::PAGE_TITILE, // Title
			array( $this, 'print_section_info' ), // Callback
			self::MENU_SLUG // Page
		);

		add_settings_field( 'email', // ID
			'CloudFlare Email Address', // Title
			array( $this, 'email_callback' ), // Callback
			self::MENU_SLUG, // Page
			'setting_section_id' // Section
		);

		add_settings_field( 'API_key', // ID
			'CloudFlare API Token', // Title
			array( $this, 'API_key_callback' ), // Callback
			self::MENU_SLUG, // Page
			'setting_section_id' // Section
		);

		add_settings_field( 'domain', // ID
			'CloudFlare Domain', // Title
			array( $this, 'domain_callback' ), // Callback
			self::MENU_SLUG, // Page
			'setting_section_id' // Section
		);
	}

	/**
	 * Sanitize each setting field as needed
	 *
	 * @param array $input Contains all settings fields as array keys
	 */
	public function sanitize( $input ) {
		$new_input = array();
		if ( isset( $input['email'] ) ) {
			$new_input['email'] = sanitize_text_field( $input['email'] );
		}

		if ( isset( $input['API_key'] ) ) {
			$new_input['API_key'] = sanitize_text_field( $input['API_key'] );
		}

		if ( isset( $input['domain'] ) ) {
			$new_input['domain'] = sanitize_text_field( $input['domain'] );
		}

		return $new_input;
	}

	/**
	 * Print the Section text
	 */
	public function print_section_info() {
		print 'Plugin purges entire cache for domain';
	}

	/**
	 * Get the settings option array and print one of its values
	 */
	public function email_callback() {
		printf( '<input class="regular-text code" type="text" id="email" name="' . self::$options_name .
		        '[email]" value="%s" />', isset( self::$options['email'] ) ? esc_attr( self::$options['email'] ) : '' );
	}

	public function API_key_callback() {
		printf( '<input autosuggest="true" class="regular-text code" type="password" id="API_key" name="' .
		        self::$options_name . '[API_key]" value="%s" />
            <button id="visibility_toggle" type="button" class="button button-secondary wp-hide-pw hide-if-no-js" data-toggle="0" data-label="Show">
                <span class="dashicons dashicons-visibility"></span>
                <span class="text">Show</span>
            </button>', isset( self::$options['API_key'] ) ? esc_attr( self::$options['API_key'] ) : '' );
	}

	public function domain_callback() {
		$domain = self::$options['domain'];
		if ( ! $domain ) {
			$domain = preg_replace( '/http:\/\//', '', get_home_url() );
			$domain = preg_replace( '/www./', '', $domain );
		}
		printf( '<input class="regular-text code" type="text" id="domain" name="' . self::$options_name .
		        '[domain]" value="%s" />', $domain );
	}

	public function purge_cf_cache() {
		$this->get_options();
		$options  = self::$options;
		$api_call = array(
			'a'     => 'fpurge_ts',
			'tkn'   => $options['API_key'],
			'email' => $options['email'],
			'z'     => $options['domain'],
			'v'   => 1
		);

		$results = wp_remote_post( 'https://www.cloudflare.com/api_json.html', array( 'headers' => '', 'body' => $api_call ) );
		echo json_encode( $results );
		die();
	}
}