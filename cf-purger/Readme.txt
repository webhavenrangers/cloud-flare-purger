=== Plugin Name ===
Contributors: webrangers
Tags: cloudflare, cache, cdn, free, performance, speed, cache clear, Cloud Flare API
Requires at least: 3.1
Tested up to: 4.5.3-alpha-37606
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Author URI: https://profiles.wordpress.org/webrangers/

Cache Purger for Cloud Flare makes clearing Cloud Flare cache simple as one click.

== Description ==

Simply purges whole CloudFlare cache for desired domain if you entered your Cloud Flare data.
Purge is done from WordPress admin panel or plugin's page. Future versions will contain functionality which purge specific URI and/or specific pages/posts

THINGS YOU NEED TO KNOW:

* Sometimes Cloud Flare Api refuses to clear the cache. In this case alert popup appears with Cloud Flare Api response.
* This version allows you to purge whole CF cache. Further versions will contain functionality for custom URIs cache purge


== Installation ==

1. Upload the plugin plugin to your blog.
2. Activate it.
3. Go to Dashboard->CF Purger (or directly to this URL: "http://your-wordpress-site.com/wp-admin/admin.php?page=cf-purger") and enter following fields:
    + CloudFlare Email Address
    + CloudFlare API Token (you can find it in your Cloud Flare <a target="_blank" href="https://www.cloudflare.com/a/account/my-account" >Account</a> ) previously logged in to CloudFlare
    + CloudFlare Domain - desired domain you want to clear cache for
4. Press "Save Changes" button
5. Be happy! :-)

== Frequently Asked Questions ==

= What if I haven't cleared CloudFlare cache but still receiving "Rate limit enforced for cache purge. (You can try again in 60 seconds.) If you'd like to make rapid changes to your cached resources, check out Development Mode" ?=

It means that Cloud Flare Api thinks that cache have been recently cleared for some reason. Retry helps.

== Screenshots ==

1. Plugin blank settings
2. Plugin settings filled with purge button
3. Cloud Flare cache being cleared with API key setting unveiled
4. Error notification popup
5. Settings page with successful purge status

== Upgrade Notice ==

= 1.0 =
Initial plugin release