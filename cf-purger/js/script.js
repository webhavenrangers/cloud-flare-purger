/**
 * Created by Phantom on 18.03.2016.
 */

(function($){
    $(document).ready(function(){

        var visibility_toggle = $('#visibility_toggle');
        visibility_toggle.click( function(){
            if( $(this).attr('data-label') == "Show" ) {
                $( 'input#API_key').attr('type', 'text' );
                $(this).attr('data-label', "Hide");
                $(this).find('.dashicons-visibility').removeClass('dashicons-visibility').addClass('dashicons-hidden');
                $(this).find('.text').html('Hide');
            } else if( $(this).attr('data-label') == "Hide" ) {
                $( 'input#API_key').attr('type', 'password' );
                $(this).attr('data-label', "Show");
                $(this).find('.dashicons-hidden').removeClass('dashicons-hidden').addClass('dashicons-visibility');
                $(this).find('.text').html('Hide');
            }
        });

        var purge_cf_button = $('#wp-admin-bar-cf-purger-clear');
        purge_cf_button.click(function(){
            var plpage_button = $('#purge_cloud_flare');
            $.ajax({
                url:		"/wp-admin/admin-ajax.php",
                type:		'POST',
                dataType:	'json',
                data:		{
                    action:	'purge_cf_cache'
                },
                beforeSend: function() {
                    purge_cf_button.find('a').toggleClass('spin');
                    if(plpage_button.length !== 0)
                        plpage_button.find('.dashicons-cloud').toggleClass('is-active spinner dashicons dashicons-cloud');
                },
                success:function( data ) {
                    var cf_answer = $.parseJSON(data.body);
                    if( cf_answer.result == 'success') {
                        purge_cf_button.find('a').toggleClass('spin').addClass('success');
                        setTimeout(function () {purge_cf_button.find('a').removeClass('success')}, 2000);
                        if(plpage_button.length !== 0) {
                            plpage_button.find('.spinner').toggleClass('is-active spinner dashicons dashicons-thumbs-up');
                            setTimeout(function () {plpage_button.find('.dashicons').toggleClass('dashicons-thumbs-up dashicons-cloud')}, 2000);
                        }
                    } else {
                        purge_cf_button.find('a').toggleClass('spin').addClass('error');
                        setTimeout(function () {purge_cf_button.find('a').removeClass('error'); alert(cf_answer.msg)}, 2000);
                        if(plpage_button.length !== 0) {
                            plpage_button.find('.spinner').toggleClass('is-active spinner dashicons dashicons-thumbs-down');
                            setTimeout(function () {plpage_button.find('.dashicons').toggleClass('dashicons-thumbs-down dashicons-cloud')}, 2000);
                        }
                    }
                },
                error: function( response ) {

                }
            });
            return false;
        });
    });
})(jQuery);